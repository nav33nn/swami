import cPickle
import csv
from collections import Counter

dictObj = cPickle.load(open('/vagrant/swami/documentType/wordList/ncr/wordDict.p', 'rb'))

sortedWL = Counter(dictObj).most_common()

with open('/vagrant/swami/documentType/data/wordList.csv','wb') as file:
	writer = csv.writer(file)
	for word,count in sortedWL:
		writer.writerow([word,count])