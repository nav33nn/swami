import csv
from nltk.tag import pos_tag, map_tag
import nltk
import cPickle

answerKey = list(csv.reader(open('/vagrant/bitbucket/swami/documentType/data/typeTitleNCR.csv')))

typeTitleList = []

[typeTitleList.append([line[1],line[2]]) for line in answerKey[1:]]

typeWordDict = {}
for line in typeTitleList:
	if line[0] in typeWordDict:
		typeWordDict[line[0]].append(line[1])
	else:
		typeWordDict[line[0]]=[line[1]]

for docType,titles in typeWordDict.iteritems():
	typeWordDict.update({docType : 	nltk.pos_tag(list(set((' '.join(titles).lower().split()))))})

cPickle.dump(typeWordDict,open('/vagrant/bitbucket/swami/documentType/data/dict.p','wb'))

for docType,titles in typeWordDict.iteritems():
	print docType, titles
	print "------------------------------"