import nltk
import glob
from nltk.tag import pos_tag, map_tag
from nltk.stem import PorterStemmer
from nltk.tag import StanfordNERTagger
import os
import string

punctuation = '#$%*+\-/;<=>?@[\\]^_`{|}~'
textFiles = glob.glob('/vagrant/data/textFiles/entities_test/*.txt')
ps = PorterStemmer()

def readFile(path):
	text = open(path).read()
	return text

def splitToSentences(text):
	# sentenceList = nltk.sent_tokenize(text.decode('utf-8'))
	sentenceList = text.split('.')
	return sentenceList

def tagSentence(sentence):
	text = nltk.word_tokenize(sentence)
	posTagged = pos_tag(text)
	return posTagged

def removePunctInString(sentence):
	sentence = sentence.translate(None, string.punctuation)
	return sentence

def removePunctsInTags(tags):
	tagsWithoutPunctLabels = []
	for tag in tags:
		if tag[0][0] not in punctuation:
			tagsWithoutPunctLabels.append(tag)
	return tagsWithoutPunctLabels


def stemSentences(sentence):
	stemmedSentence = [sentence.stem(word) for word in sentence.encode('utf-8').split()]
	return ' '.join(stemmedSentence)

def nerTagger(sentence):
	stanford_classifier = os.environ.get('STANFORD_MODELS').split(':')[0]
	stanford_ner_path = os.environ.get('CLASSPATH').split(':')[0]
	st = StanfordNERTagger(stanford_classifier, stanford_ner_path, encoding='utf-8')
	return st.tag(sentence.split())


if __name__ == "__main__":
	for file in textFiles:
		content = readFile(file)
		contentSentences = splitToSentences(content)
		for sentence in contentSentences[:100]:
			print sentence
			sentence = removePunctInString(sentence)
			tags = tagSentence(sentence)
			ners = nerTagger(sentence)
			for tag, ner in zip(tags,ners):
				print tag , ner
			print "----------------------------------"