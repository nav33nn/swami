import cPickle as pickle
from bs4 import BeautifulSoup as bs
import glob
import string
from collections import Counter as ctr
from decimal import Decimal
from nltk.tag import StanfordNERTagger
from nltk.tag import pos_tag, map_tag
import os
import nltk
from nltk.tokenize import word_tokenize

punctuation = string.punctuation
xmlFilePath = '/vagrant/data/xmlFiles/entities_test/*.xml'
wordList = list(set(pickle.load(open('/vagrant/bitbucket/swami/entities/data/wordList.pickle','rb'))))
xmlFiles = glob.glob(xmlFilePath)

def getXMLpageObjects(filePath):
	f = open(filePath)
	soup = bs(f,'xml')
	pages = soup.findAll('page')
	return pages

def getParaStats(textContent,wordList,para):
	wordCountList = []
	matches = 0	
	textContentA = ' '.join(list(set(textContent.split())))
	totalWordsUnique = len(list(set(textContent.split())))
	for word in wordList:
		if word in textContentA:
			matches= matches+1
	if matches == 0:
		return 0,0,0
	else:
		score = float(totalWordsUnique)/float(matches)
	return matches, totalWordsUnique, score


def getParasWithEntities(pageStatList,lowerLimit,upperLimit):
	confidentParas = []
	for pageStats in pageStatList:
		for paraStats in pageStats:
			if paraStats[4] > lowerLimit and paraStats[4] < upperLimit:
				confidentParas.append(paraStats)
	return confidentParas

def removePunctInString(sentence):
	sentence = sentence.translate(None, string.punctuation)
	return sentence

def nerTagger(sentence):
	stanford_classifier = os.environ.get('STANFORD_MODELS').split(':')[0]
	stanford_ner_path = os.environ.get('CLASSPATH').split(':')[0]
	st = StanfordNERTagger(stanford_classifier, stanford_ner_path, encoding='utf-8')
	return st.tag(sentence)


def posTagger(sentence):
	text = nltk.word_tokenize(sentence)
	posTagged = pos_tag(text)
	return posTagged

def getEntities_N(confidentParas, n):
	for paraStats in confidentParas[:4]:
		text = str(paraStats[5].encode("utf-8"))
		textWithoutPunct = removePunctInString(text)
		sentences = text.split('.')
		nerTagsList = []
		# for sentence in sentences:
		# 	print word_tokenize(sentence)
		# 	print nerTagger(word_tokenize(sentence))
		# nerTags = nerTagger(text)
		posTags = posTagger(textWithoutPunct)
		print posTags , len(posTags)



if __name__ == '__main__':
	for xmlFile in xmlFiles:
		xmlPages = getXMLpageObjects(xmlFile)
		pageStatList = []
		for xmlPage in range(len(xmlPages)):
			paras = xmlPages[xmlPage].findAll('para')
			paraStatList = []
			for para in range(len(paras))[1:-1]:
				# textContent = ' '.join(paras[para-1].text.lower().split())+ ' '+' '.join(paras[para].text.lower().split())+ ' '+' '.join(paras[para+1].text.lower().split())
				textContent = ' '.join(paras[para-1].text.split())+ ' '+' '.join(paras[para].text.split())+ ' '+' '.join(paras[para+1].text.split())
				matches, totalWordCount, paraScore = getParaStats(textContent,wordList,para)
				if totalWordCount > 10:
					paraStatList.append([xmlPage,para,matches,totalWordCount, paraScore, textContent])
			pageStatList.append(paraStatList)
		confidentParas = getParasWithEntities(pageStatList,0.9,1.1)
		entities = getEntities_N(confidentParas,3)
		print entities