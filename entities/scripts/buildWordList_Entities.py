from bs4 import BeautifulSoup as bs
import glob
import csv
from collections import Counter as ctr
from pympler import asizeof
import cPickle as pickle
import enchant
import string
import nltk 
from nltk.tag import pos_tag, map_tag


punctuation = string.punctuation
dictionary = enchant.Dict("en_US")
xmlFilePath = '/vagrant/data/xmlFiles/entities/' #------------> Reference files
# xmlKeys = list(csv.reader(open('/vagrant/bitbucket/swami/entities/data/entitiesTest.csv')))
xmlKeys = list(csv.reader(open('/vagrant/bitbucket/swami/entities/data/entities.csv'))) #----------> Input file
wordListFilePath = '/vagrant/bitbucket/swami/entities/data/wordList.pickle'
flatListFilePath = '/vagrant/bitbucket/swami/entities/data/flat_list.pickle'

#      Pramata_no	X	X2	Y	Y2	max_width	max_height	page_number
# above mentioned is the csv format for the input file

def getXMLpageObjects(filePath):
	f = open(filePath)
	soup = bs(f,'xml')
	pages = soup.findAll('page')
	return pages

def getPageWidthHeight(pageObject):
	theoPage = pageObject.findAll('theoreticalPage')
	width = str(theoPage[0]['width'])
	height = str(theoPage[0]['height'])
	return width, height

def getParaObjectFromCoordinates(pageObject, co_ordsInfo, pageNo):
	paras = pageObject.findAll('para')
	xmlWidth , xmlHeight = getPageWidthHeight(pageObject)
	requiredParas = []
	for para in range(len(paras)):
		l = float(paras[para]['l'])
		t = float(paras[para]['t'])
		r = float(paras[para]['r'])
		b = float(paras[para]['b'])
		imageWidth = float(co_ordsInfo[5])
		imageHeight = float(co_ordsInfo[6])
		heightRatio = float(xmlHeight)/float(imageHeight)
		widthRatio = float(xmlWidth)/float(imageWidth)
		x = float(co_ordsInfo[1])*widthRatio
		x2 = float(co_ordsInfo[2])*widthRatio
		y = float(co_ordsInfo[3])*heightRatio
		y2 = float(co_ordsInfo[4])*heightRatio
		if l>x and t>y and r>x2 and b>y2:
			requiredParas.append([pageNo, para, str(' '.join(paras[para].text.split()))])
	return requiredParas


def removeWordsWithTags(taggedList,tag):
	cleanedTagList = []
	removedWordTags = []
	for tagWord in taggedList:
		if tagWord[1] not in tag:
			cleanedTagList.append(tagWord)
		else:
			removedWordTags.append(tagWord)
	return cleanedTagList, removedWordTags


def getWordsFromTags(tagList):
	wordList = []
	for tag in tagList:
		wordList.append(tag[0])
	return wordList


if __name__ == '__main__':
	# allDocText = []
	# for co_ordsInfo in xmlKeys[1:]:
	# 	try:
	# 		xmlPages = getXMLpageObjects(xmlFilePath+co_ordsInfo[0]+'.xml')
	# 		pageOfInterest = xmlPages[int(co_ordsInfo[-1])-1]
	# 		paraObject = getParaObjectFromCoordinates(pageOfInterest,co_ordsInfo,int(co_ordsInfo[-1])-1)
	# 		print paraObject
	# 		intermediateText = []
	# 		for item in paraObject:
	# 			text = item[2]
	# 			intermediateText.append(text)
	# 		allDocText.append(intermediateText)
	# 	except:
	# 		continue
	# flatList = [item for sublist in allDocText for item in sublist]
	# with open('/vagrant/bitbucket/swami/entities/data/allDocText.pickle','wb') as fp:
	# 	pickle.dump(allDocText,fp)
	# with open(flatListFilePath,'wb') as fp1:
	# 	pickle.dump(flatList,fp1)


	#--------------------------------uncomment above lines on live data------------------------

	flatList = pickle.load(open(flatListFilePath,'rb'))
	tokenList = []
	tokenString = ' '.join(flatList)
	tokenString = tokenString.translate(None,punctuation)
	# for item in flatList:
	# 	tokenList.append(item.split())
	for item in tokenString.split():
		tokenList.append(item.split())
	tokenList = [item for sublist in tokenList for item in sublist if dictionary.check(item) and not item.isdigit()]
	tokenList = list(set(tokenList))
	
	taggedWordList = nltk.pos_tag((' '.join(tokenList)).lower().split())

	cleanedTagList, removedTagList = removeWordsWithTags(taggedWordList,['NN','NNP','NNPS','NNS','FW','LS','PRP','UH','JJ','JJR','LS'])
	wordList = getWordsFromTags(cleanedTagList)

	with open(wordListFilePath,'wb') as wL:
		pickle.dump(wordList,wL)



# text tokenized after removing puncts. 
# next step --> Identify the appropriate words to point to the paragraph where the entity is present

