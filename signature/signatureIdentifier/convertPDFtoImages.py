from __future__ import print_function
from wand.image import Image
import glob
import subprocess
import os

pdfFiles = glob.glob('/vagrant/data/pdfFiles/NCR/*.pdf')
outputPath = '/vagrant/data/pdfFiles/convertedImages/'
for pdfFile in pdfFiles:
    fileName = pdfFile
    print (fileName)
    pramataNo = pdfFile.split('/')[-1].split('.')[0]
    print (pramataNo)
    # subprocess.call(["convert -verbose -density 200 -resize 800 {fileName} {pramataNo}_%d.png"])
    os.system("convert -verbose -density 200 -resize 800 "+fileName+" "+outputPath+pramataNo+"_%d.png")