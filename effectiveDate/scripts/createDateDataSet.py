import csv
from bs4 import BeautifulSoup as bs
import glob
import itertools
import re
from dateutil.parser import parse
from collections import Counter as ctr



wordList = [val for sublist in list(csv.reader(open('/vagrant/bitbucket/swami/effectiveDate/output/wordList.csv'))) for val in sublist]

xmlFiles = glob.glob('/vagrant/data/dateFiles_test/*.xml')
# xmlFiles = glob.glob('/vagrant/data/dateFiles/*.xml')
# xmlFiles = glob.glob('/vagrant/bitbucket/swami/effectiveDate/test_set/*.xml')

regexes = ['(?:(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)|Aug(?:ust)?|Sep(?:t(?:ember)?)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)|0?[1-9]|1[012])[- \\/.,][ ]*(?:(?:[0-9]{1,2}))[ ]*(?:st|nd|rd|th)?[- /.,][ ]*[ 0-9]{1,2}[0-9]{1,2}','(?:(?:[0-9]{1,2}))[ ]*(?:st|nd|rd|th)?[- \/.,][ ]*(?:day of\s+)?(?:(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)|Aug(?:ust)?|Sep(?:t(?:ember)?)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)|0?[1-9]|1[012])[- \\/.,][ ]*[ 0-9]{1,2}[0-9]{1,2}','[0-9]{4}[- \/.,][ ]*(?:(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)|Aug(?:ust)?|Sep(?:t(?:ember)?)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)|0?[1-9]|1[012])[- \/.,][ ]*(?:(?:[0-9]{1,2}))[ ]*(?:st|nd|rd|th)?']




def docHash(file):	
	f = open(file)
	soup = bs(f,'xml')
	pages = soup.findAll('page')
	docHash = {}
	for page in range(len(pages)):
		paras = pages[page].findAll('para')
		paraList = {}
		for para in range(len(paras)):
			finding = []
			l = paras[para]['l']
			t = paras[para]['t']
			r = paras[para]['r']
			b = paras[para]['b']
			z = ' '.join(paras[para].text.split())

			#---------------------------------------- find dates based on RE-----------------

			datesFound, dates = findDates(z)
			# matches = list(datefinder.find_dates(z))
			paraContentList = paras[para].text.split()
			paraContentList = [x.encode('utf-8') for x in paraContentList]
			wordMatches = set(paraContentList) & set(wordList)
			finding.append([dates,l,t,r,b,page,para,len(wordMatches)])
			paraList[para]= paraContentList,len(paraContentList),finding
			docHash[page] = paraList
	return docHash


def findDates(paraContent):
	combined = "(" + ")|(".join(regexes) + ")"
	matchedDateList = re.findall(combined,paraContent)
	dateFound = 0
	for matches in range(len(matchedDateList)):
		if len(matchedDateList[matches][0])>0 or len(matchedDateList[matches][1])>0 or len(matchedDateList[matches][2])>0:
			dateFound = 1
	return dateFound, matchedDateList


def listOfDatesWithParaContent(file):
	documentDatePantomath = []
	f = open(file)
	soup = bs(f,'xml')
	pages = soup.findAll('page')
	for page in range(len(pages)):
		pageNo = page + 1
		paras = pages[page].findAll('para')
		for para in range(len(paras)):
			paraNo = para + 1
			paraContent =' '.join(paras[para].text.split())
			dateFound, matchedDateList = findDates(paraContent)
			try:
				if dateFound == 1:
					documentDatePantomath.append([pageNo, paraNo, matchedDateList, paras[para-1], paras[para], paras[para+1]])
			except:
				if dateFound == 1:
					documentDatePantomath.append([pageNo, paraNo, matchedDateList, paras[para-1], paras[para]])
	return documentDatePantomath


def buildWordVector(dateObject, wordList):
	wordVec = []
	prevParaContent = ' '.join(dateObject[3].text.split())
	currentParaContent = ' '.join(dateObject[4].text.split())
	try:	
		nextParaContent = ' '.join(dateObject[5].text.split())
		textContent = prevParaContent+' '+currentParaContent+' '+nextParaContent
	except:
		textContent = prevParaContent+' '+currentParaContent
		pass
	for word in wordList:
		wordCount = textContent.split().count(word)
		wordVec.extend(str(wordCount))
	return wordVec, textContent



def getDatesFromDateObject(dateObject):
	dateList = dateObject[2]
	dates = []
	for date in dateList:
		try:	
			dt = parse(date[0])
			dates.append(dt)
		except:
			pass
	return dates


def getPageWidthHeight(path, pageNo):
	f = open(path)
	soup = bs(f,'xml')
	pages = soup.findAll('page')
	theoPage = pages[pageNo].findAll('theoreticalPage')
	# print theoPage
	width = str(theoPage[0]['width'])
	height = str(theoPage[0]['height'])
	return width, height


def get_distance(w1, w2):
	if w1 in wordList and w2 in wordList:
		w1_indexes = [index for index, value in enumerate(wordList) if value == w1]
		w2_indexes = [index for index, value in enumerate(wordList) if value == w2]
		distances = [abs(item[0] - item[1]) for item in itertools.product(w1_indexes, w2_indexes)]
		return {'min': min(distances), 'avg': sum(distances)/float(len(distances))}
	else:
		return 0



# def addDistanceToWordListVector(dateList, wordVector, wordList, content):
# 	for index, word in enumerate(wordList):





def buildDataSet(documentDatePantomath, wordList, filePath):
	allDateVector = []
	pramataNo = filePath.split('/')[-1].split('.')[0]

	
	for dateObject in documentDatePantomath:
		
		matchedDates = [str(item) for sublist in dateObject[2] for item in sublist if len(item)>1]
		# print matchedDates


		wordVector, content = buildWordVector(dateObject, wordList)
		# word = addDistanceToWordListVector(dateObject[2], wordVector, wordList, content)
		dates = getDatesFromDateObject(dateObject)
		pageNo = dateObject[0]
		width, height = getPageWidthHeight(filePath, pageNo-1)

		paraNo = dateObject[1]
		for date in dates:
			for matchedDate in matchedDates:
				
				try:
					if parse(matchedDate) == date:
						paraObj = dateObject[4]
						lines = paraObj.findAll('ln')
						lineL = 0
						lineT = 0
						lineR = 0
						lineB = 0
						for line in lines:
							if matchedDate in ' '.join(line.text.split()):
								lineL = line['l']
								lineT = line['t']
								lineR = line['r']
								lineB = line['b']

								isMaxDate = 0
								if date == max(dates):
									isMaxDate = 1
								try:		
									lineItem = [[pramataNo],[pageNo],[paraNo],[width],[height],[lineL],[lineT],[lineR],[lineB],[date.strftime('%m/%d/%Y')],wordVector,[str(isMaxDate)]]
									allDateVector.append([item for sublist in lineItem for item in sublist])
								except:
									pass
				except:
					pass
	
	return allDateVector



if __name__ == "__main__":
	print "total files : ", len(xmlFiles)
	cnt = 0
	finalDataSet = []
	for filePath in xmlFiles:
		cnt = cnt + 1
		print cnt, filePath
		documentDatePantomath = listOfDatesWithParaContent(filePath)
		# print docObject
		dataSet = buildDataSet(documentDatePantomath, wordList, filePath)

		# print dataSet
		finalDataSet.extend(dataSet)
	

	# print finalDataSet
	header = [['Pramata No'], ['Page No'], ['Para No'],['width'],['height'],['lineL'],['lineT'],['lineR'],['lineB'],['Date'], wordList, ['isMaxDate']]
	header = [item for sublist in header for item in sublist]
	with open('/vagrant/bitbucket/swami/effectiveDate/output/dataSet_test.csv', 'wb') as file:
		writer = csv.writer(file)
		writer.writerow(header)
		for line in finalDataSet:
			writer.writerow(line)

			# dateList = docObject[match][2]
			# for date in dateList:
			# 	try:
			# 		dt = parse(date[0])
			# 		print dt
			# 		print dt.strftime('%m/%d/%Y')
			# 	except:
			# 		pass

























		# maxMatchCount = 0
		# pageOfInterest = 0
		# paraOfInterest = 0
		# l = 0
		# t = 0
		# r = 0
		# b = 0
		# dates = []
		# for item,a in docObject.iteritems():
		# 	for z,c in a.iteritems():
		# 		# print c[2][0], c[2][0][4]
		# 		if c[2][0][7] > maxMatchCount:
		# 			maxMatchCount = c[2][0][7]
		# 			pageOfInterest = c[2][0][5]
		# 			paraOfInterest = c[2][0][6]
		# 			dates = c[2][0][0]
		# 			l = c[2][0][1]
		# 			t = c[2][0][2]
		# 			r = c[2][0][3]
		# 			b = c[2][0][4]
		# print maxMatchCount, pageOfInterest, paraOfInterest, dates, l, t, r, b


