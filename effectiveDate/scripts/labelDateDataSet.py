import csv
from dateutil.parser import parse


csvFile = list(csv.reader(open('/vagrant/bitbucket/swami/effectiveDate/data/ncr_effectiveDates.csv')))
dataSet = list(csv.reader(open('/vagrant/bitbucket/swami/effectiveDate/output/dataSet.csv')))
dateAnswerKey = dict(csvFile)
dateTrainingSetPath = '/vagrant/bitbucket/swami/effectiveDate/trainingSet/effDate_train_final.csv'

with open(dateTrainingSetPath,'wb') as trainSet:
	writer = csv.writer(trainSet)
	header = dataSet[0]
	header.extend(['isCorrect'])
	writer.writerow(header)
	for line in dataSet[1:]:
		pramataNo = line[0]
		date = line[9]
		correctDate = dateAnswerKey[pramataNo]
		# print parse(correctDate), parse(date), parse(correctDate)== parse(date)
		if parse(correctDate) == parse(date):
			line.extend(['True'])
		else:
			line.extend(['False'])
		writer.writerow(line)


