import csv
import datefinder
from bs4 import BeautifulSoup as bs
import glob
from nltk.corpus import stopwords
import string
import numpy
import pandas
import enchant
from collections import Counter

skynetDataLoc = '/vagrant/swami/effectiveDate/data/effectiveDate_skynetReport.csv'
skynetData = list(csv.reader(open(skynetDataLoc)))
xmlFolderLoc = '/vagrant/data/dateFiles/'
cleanData = []
stop = set(stopwords.words('english'))
enchDict = enchant.Dict("en_US")
punct = string.punctuation



for line in skynetData:
	dataDict = {}
	matches = list(datefinder.find_dates(line[1]))
	# cleanData.append(matches)
	try:
		if matches:
			# print "inside if"
			dataDict['poNumber'] = line[0]
			dataDict['content'] = line[1]
			dataDict['page'] = line[2]
			dataDict['l'] = line[3]
			dataDict['t'] = line[4]
			dataDict['r'] = line[5]
			dataDict['b'] = line[6]
			dataDict['width'] = line[7]
			dataDict['height'] = line[8]
	except:
		pass
	cleanData.append(dataDict)


cleanData = [item for item in cleanData if item]

with open('/vagrant/swami/effectiveDate/output/wordList.csv','wb') as fil:
	writer = csv.writer(fil)
	contentList = []
	for entry in cleanData:
		poNumber = entry['poNumber']
		# if poNumber == '303096':
		# print type(poNumber)
		# print poNumber
		dl = int(float(entry['l']))
		dt = int(float(entry['t']))
		dr = int(float(entry['r']))
		db = int(float(entry['b']))
		dwidth = int(float(entry['width']))
		dheight = int(float(entry['height']))
		# print dwidth, dheight
		page = entry['page']
		filePath = xmlFolderLoc+poNumber+'.xml'
		try:	
			f = open(filePath)
			soup = bs(f,'xml')
			pages = soup.findAll('page')
			pageOfInterest = pages[int(page)-1]
			theoPage = pageOfInterest.findAll('theoreticalPage',{"width":True})
			# paras = pageOfInterest.findAll('para')
			paras=soup.findAll('para',{"l":True})
			for para in paras:
				if entry['content'] in ' '.join(para.text.split()):
					contentList.append(' '.join(para.text.split()))
					break
					'''cl = int(float(para['l']))
					ct = int(float(para['t']))
					cr = int(float(para['r']))
					cb = int(float(para['b']))
					cwidth = int(float(theoPage[0]['width']))
					cheight = int(float(theoPage[0]['height']))
					# print para.text
					print cl , dl
					print ct , dt
					print cr , dr
					print cb , db
					print "*************************"




					dl = (cl*cheight)/dheight
					dt = (ct*cwidth)/dwidth
					dr = (cr*cheight)/dheight
					db = (cb*cwidth)/dwidth

					print cl , dl
					print ct , dt
					print cr , dr
					print cb , db
					print "--------------------------------------------------------------------------"
					if dl >= cl and dt <= ct and dr <= cr and db <= cb:
						# print "inside"
						paraOfInterest = para
						# print poNumber
						print paraOfInterest.text
						'''
		except:
			pass
	contentList = [sublist for sublist in contentList]
	# print contentList
	print len(contentList)
	contentList = ([i for i in ' '.join(contentList).lower().split(" ") if i not in stop])
	contentList = [''.join(c for c in s if c not in string.punctuation) for s in contentList]
	contentList = [word for word in ' '.join(contentList).split() if not word.isdigit() and word <> '' and len(word) > 3 and enchDict.check(word)]
	# print contentList
	countWordList = Counter(contentList)
	# print countWordList
	# contentList = list(set(contentList))
	# contentList = [x.encode('utf-8') for x in contentList]
	# countWordList = [x.encode('utf-8') for x in countWordList]

	# for item, count in countWordList.iteritems():
	# 	print item, count

	for item, count in countWordList.iteritems():
		if count > 10:
			writer.writerow([item])